import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'tab4/:tipo', loadChildren: './pages/tab4/tab4.module#Tab4PageModule' },
  { path: 'requisitos-previos', loadChildren: './pages/requisitos-previos/requisitos-previos.module#RequisitosPreviosPageModule' },
  { path: 'orden-mat', loadChildren: './pages/orden-mat/orden-mat.module#OrdenMatPageModule' },
  { path: 'why-software', loadChildren: './pages/why-software/why-software.module#WhySoftwarePageModule' },
  { path: 'campo-laboral', loadChildren: './pages/campo-laboral/campo-laboral.module#CampoLaboralPageModule' },
  { path: 'deberia', loadChildren: './pages/deberia/deberia.module#DeberiaPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
