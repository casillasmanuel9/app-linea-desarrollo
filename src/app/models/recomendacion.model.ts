export class Recomendacion {
    nombre: string;
    tipo:string;
    urlMedia:string;

    constructor(nombre, tipo, link)
    {
        this.nombre = nombre;
        this.tipo = tipo;
        this.urlMedia = link;
    }
}