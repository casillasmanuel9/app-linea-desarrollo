export class Unidad {
    nombre?: string;
    tipo?: string;
    ht?: number;
    hp?: number;
    th?: number;
    cr?: number;
    prerequisitos?: string;
    linkImg?: string;
    redirect: string;
  
    constructor(nombre, tipo, ht, hp, th, cr, pre, linkImg: string, redirect:string) {
      this.nombre = nombre;
      this.tipo = tipo;
      this.ht = ht;
      this.hp = hp;
      this.th = th;
      this.cr = cr;
      this.prerequisitos = pre;
      this.linkImg = linkImg;
      this.redirect = redirect;
    }
  }
  