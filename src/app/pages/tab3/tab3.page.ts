import { Component } from '@angular/core';
import { Recomendacion } from 'src/app/models/recomendacion.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  recomendaciones:Recomendacion[] = [
    new Recomendacion('Requisitos y conocimientos previos','a','/requisitos-previos'),
    new Recomendacion("Orden para las materias",'a','/orden-mat'),
    new Recomendacion("¿Por qué elegir desarrollo de software?",'v','/why-software'),
    new Recomendacion("¿Qué es lo que haré en el campo laboral?",'v','/campo-laboral'),
    new Recomendacion("¿Deberías elegir desarrollo de software?",'a','/deberia'),
  ];

  constructor(private navCrtl: NavController) {}

  onClick()
  {
    this.navCrtl.navigateForward(['/tab4','consejosLista']);
  }

}
