import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requisitos-previos',
  templateUrl: './requisitos-previos.page.html',
  styleUrls: ['./requisitos-previos.page.scss'],
})
export class RequisitosPreviosPage implements OnInit {

  array: any[] = [
    {
      nombre: '1- Interacción social',
      contenido: 'Existe un prejuicio común acerca de los programadores en el que se les tilda de antisociales, pero lo cierto es que son personas que deben salir de su burbuja constantemente y reunirse con sus colegas para discutir problemas de código, o nuevas aplicaciones, o ideas. También son empresarios, pues de otra forma nunca conseguirán clientes, por lo que es necesario que sepan tratar con sus clientes y explicarles cosas sobre programación de manera sencilla de comprender. Un buen programador es capaz de concentrarse en su trabajo sin dejar de lado la interacción humana.'
    },
    {
      nombre: '2- Análisis de problemas',
      contenido: 'Un programador no es nadie si no tiene la capacidad de analizar muy bien cualquier problema que encuentre en el camino. Los programadores son por naturaleza personas atentas al detalle que pueden pasar horas trabajando en un solo código y resolviendo un problema muy pequeño pero que puede terminar siendo crucial. De igual forma, a la hora de elegir información que va en una página web o una aplicación, el programador debe analizar toda la información para complacer al cliente y a la audiencia.'
    },
    {
      nombre: '3- Creatividad',
      contenido: 'Un programador es creativo por naturaleza, pues debe imaginarse el código, el software, el diseño, y más acerca de su trabajo. De igual forma, un programador no trabaja en un horario laboral común o estándar, de hecho a veces trabaja a deshoras y cuando es necesario. Por lo mismo, es importante tener la capacidad de inventar cosas en cualquier momento, de crear soluciones rápidas, y de encontrar caminos diferentes para llegar a una mejor resultado.'
    },
    {
      nombre: '4- Curiosidad',
      contenido: '¿Qué hay de los programadores si no tienen una inquietud por inventar algo nuevo? Nada. La realidad es que la curiosidad es esencial en el trabajo de un programador, pues es el combustible que los ayuda a crear nuevas aplicaciones móviles, un nuevo código mejorado, un formato más atractivo etc., dado que con la curiosidad siempre tendrán la duda acerca de qué les hace falta en su trabajo y en su área.'
    },
    {
      nombre: '5- Aprendizaje',
      contenido: 'Como en muchas carreras profesionales, la programación es un sector que cambia constantemente, por lo que los programadores deben aprender cosas nuevas todos los días. La capacidad de expandir su aprendizaje y memorización es fundamental si se quiere ser un buen programador. Es importante leer noticias, interactuar con la comunidad de programación, hacer preguntas, leer código, tomar cursos, y mucho más, para lograr ser un programador de calidad superior que sepa dar mejores resultados.'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
