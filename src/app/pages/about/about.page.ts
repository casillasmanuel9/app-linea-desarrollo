import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(private iab: InAppBrowser) { }

  ngOnInit() {
  }

  openGit() {/*
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    };
    this.document.viewDocument('assets/ALP.pdf','application/pdf', options);
    console.log('holo');*/
    const browser = this.iab.create('https://gitlab.com/casillasmanuel9?nav_source=navbar', '_black');
  }

  openCode() {
    const browser = this.iab.create(' https://gitlab.com/casillasmanuel9/app-linea-desarrollo', '_black');
  }
}
