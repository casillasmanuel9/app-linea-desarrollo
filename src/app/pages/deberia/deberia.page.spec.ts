import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeberiaPage } from './deberia.page';

describe('DeberiaPage', () => {
  let component: DeberiaPage;
  let fixture: ComponentFixture<DeberiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeberiaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeberiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
