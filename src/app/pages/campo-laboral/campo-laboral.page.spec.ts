import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampoLaboralPage } from './campo-laboral.page';

describe('CampoLaboralPage', () => {
  let component: CampoLaboralPage;
  let fixture: ComponentFixture<CampoLaboralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampoLaboralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampoLaboralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
