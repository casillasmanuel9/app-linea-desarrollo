import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-campo-laboral',
  templateUrl: './campo-laboral.page.html',
  styleUrls: ['./campo-laboral.page.scss'],
})
export class CampoLaboralPage implements OnInit {
  
  tipo = 'none';

  labores = [
    "Reunirse con los clientes y Gerentes de Proyecto para diseñar y desarrollar nuevos programas.",
    "Establecer parámetros y diseñar la arquitectura de nuevos programas.",
    "Diseñar, escribir, leer, probar y corregir el código de nuevos programas.",
    "Realizar pruebas de medición de calidad y detectar errores en el desarrollo del programa.",
    "Preparar la documentación necesaria para programas nuevos o actualizados."
  ]

  constructor(private plt: Platform) { }

  ngOnInit() {
    if(this.plt.is('android'))
    {
      this.tipo = 'android';
    }else if(this.plt.is('desktop'))
    {
      this.tipo = 'desktop';
    }
  }

}
