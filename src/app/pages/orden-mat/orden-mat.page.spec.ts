import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMatPage } from './orden-mat.page';

describe('OrdenMatPage', () => {
  let component: OrdenMatPage;
  let fixture: ComponentFixture<OrdenMatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdenMatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdenMatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
