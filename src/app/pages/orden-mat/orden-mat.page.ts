import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orden-mat',
  templateUrl: './orden-mat.page.html',
  styleUrls: ['./orden-mat.page.scss'],
})
export class OrdenMatPage implements OnInit {

  materias = [
    "Lenguaje de programación estructurado",
    "Métricas de software",
    "Desarrollo multimedia",
    "Lenguaje de programación visual",
    "Análisis de lenguajes de programación",
    "Lenguaje de programación orientada a objetos",
    "Minería de datos"
  ];

  constructor() { }

  ngOnInit() {
  }

}
