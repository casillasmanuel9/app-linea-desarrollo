import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  /*slides: { img: string, titulo: string, desc: string }[] = [
    {
      img: '/assets/slides/speaker.svg',
      titulo: 'Reproduccion',
      desc: 'Pulsa una vez en el boton de audio para reproducirlo, y dos para detenerlo.'
    },
    {
      img: '/assets/slides/video.svg',
      titulo: 'Video',
      desc: 'Para poder visualizar mejor el video, activa la horientacion y pon tu celular de manera horizontal.'
    },
    {
      img: '/assets/slides/audio.svg',
      titulo: 'Audio',
      desc: 'Usa audifonos para mejorar tu experiencia.'
    }
  ];*/

  slides: { img: string, titulo: string, desc: string }[] = [];
  tipo: string;
  tipoPlataforma = 'nel';

  constructor(private alertController: AlertController, private iab: InAppBrowser, private navCtrl: NavController, private activeRouter: ActivatedRoute) { }

  ngOnInit() {

    this.tipo = this.activeRouter.snapshot.paramMap.get('tipo');
    if (this.tipo == 'consejos') {
      this.slides = [
        {
          img: '/assets/slides/video.svg',
          titulo: 'Video',
          desc: 'Para poder visualizar mejor el video, activa la orientación y pon tu celular de manera horizontal.'
        },
        {
          img: '/assets/slides/audifonos.svg',
          titulo: 'Audio',
          desc: 'Usa audífonos para mejorar tu experiencia.'
        }
      ];
    } else if (this.tipo == 'planes') {
      this.slides = [
        {
          img: '/assets/slides/wifi.svg',
          titulo: 'Planes',
          desc: 'Para poder ver los planes, necesitas una conexión a wifi.'
        },
        {
          img: '/assets/slides/click.svg',
          titulo: 'Ver',
          desc: 'Da click a la imagen de alguna materia para ver su plan'
        }
      ];
    } else if (this.tipo == 'consejosLista') {
      this.slides = [
        {
          img: '/assets/slides/click.svg',
          titulo: 'Ver',
          desc: 'Selecciona un item de la lsita para acceder a la información'
        }
      ];
    }
  }

  async developer() {
    const alert = await this.alertController.create({
      header: 'Desarrollado Por',
      message: 'Juan Manuel Galindo Casillas.',
      buttons: ['OK']
    });

    await alert.present();
  }

  openGit() {/*
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    };
    this.document.viewDocument('assets/ALP.pdf','application/pdf', options);
    console.log('holo');*/
    const browser = this.iab.create('https://gitlab.com/casillasmanuel9?nav_source=navbar', '_black');
  }

  onClick() {
    this.navCtrl.pop();
  }

}
