import { Component } from '@angular/core';
import { Unidad } from '../../models/unidad.model';
import { DocumentViewerOptions, DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  

  url = '/assets/images/';
  unidades: Unidad[] = [
    new Unidad('Métricas de Software', 'Curso', 2, 2, 4, 6, 'Ninguno', this.url + 'ms.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Programacion_e_ingenieria_de_software/Metricas_de_Software.pdf'),
    new Unidad('Análisis de Lenguajes de Programación', 'Curso', 2, 2, 4, 6, 'Ninguno', this.url + 'alp.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Programacion_e_ingenieria_de_software/Analisis_de_Lenguajes_de_Programacion.pdf'),
    new Unidad('Lenguaje de Programación Estructurado', 'Curso y Laboratorio', 2, 1, 3, 5, 'Programacion Estructurada', this.url + 'lpe.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Programacion_e_ingenieria_de_software/Lenguaje_de_Programacion_Estructurado_2013.pdf'),
    new Unidad('Lenguaje de Programación Visual', 'Curso y Laboratorio', 2, 1, 3, 5, 'Programacion Estructurada', this.url + 'lpv.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Programacion_e_ingenieria_de_software/Lenguaje_de_Programacion_Visual.pdf'),
    new Unidad('Lenguaje de Programación Orientada a Objetos', 'Curso y Laboratorio', 2, 1, 3, 5, 'Programacion Orientada a Objetos', this.url + 'lpoo.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Programacion_e_ingenieria_de_software/Lenguaje_de_Programacion_Orientado_a_Objetos.pdf'),
    new Unidad('Minería de Datos', 'Curso', 2, 2, 4, 6, 'Fundamentos de Bases de Datos', this.url + 'md.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Tratamiento_de_la_Informacion/Mineria_de_Datos.pdf'),
    new Unidad('Desarrollo Multimedia', 'Curso y Laboratorio', 1, 2, 3, 4, 'Multimedia', this.url + 'mul.png','http://fingenieria.uaemex.mx/portal/docs/coordinaciones/ICO/planF2/Interaccion_Hombre_Maquina/Desarrollo_Multimedia.pdf'),
  ];


  constructor(private document: DocumentViewer,private fileOpener: FileOpener, private iab:InAppBrowser, private navCrtl:NavController) { }

  verPdf(url:string)
  {/*
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    };
    this.document.viewDocument('assets/ALP.pdf','application/pdf', options);
    console.log('holo');*/
    const browser = this.iab.create(url,'_black');
  }

  onClick()
  {
    this.navCrtl.navigateForward(['/tab4','planes']);
  }

}
