import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReproductorComponent } from './reproductor/reproductor.component';
import { ReproductorAudioComponent } from './reproductor-audio/reproductor-audio.component';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [ReproductorComponent, HeaderComponent, ReproductorAudioComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ReproductorComponent,
    HeaderComponent,
    ReproductorAudioComponent,
  ]
})
export class ComponentsModule { }
