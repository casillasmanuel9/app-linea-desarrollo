import { Component, OnInit, Input } from '@angular/core';
import { VideoPlayer, VideoOptions } from '@ionic-native/video-player/ngx';

@Component({
  selector: 'app-reproductor',
  templateUrl: './reproductor.component.html',
  styleUrls: ['./reproductor.component.scss'],
})
export class ReproductorComponent implements OnInit {
  
  @Input() urlVideo:string;
  videoOption: VideoOptions;

  constructor(private videoPlayer: VideoPlayer) { }

  ngOnInit() {}

  playVideo() {
    this.videoOption = {
      scalingMode: 25,
      volume: 1.0
    }
    try {
      this.videoPlayer.play('file:///android_asset/www/assets/videos/'+this.urlVideo,this.videoOption).then(() => {
        console.log('video completed');
      }).catch(err => {
        console.log(err);
      });

    } catch (e) {

    }
  }

}
