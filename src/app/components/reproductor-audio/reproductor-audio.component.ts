import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-reproductor-audio',
  templateUrl: './reproductor-audio.component.html',
  styleUrls: ['./reproductor-audio.component.scss'],
})
export class ReproductorAudioComponent implements OnInit {

  @Input() urlAudio: string;
  estado: number = 0;

  constructor(private nativeAudio: NativeAudio) { }

  ngOnInit() {
  }

  playAudio() {
    if (this.estado == 0) {
      this.nativeAudio.preloadSimple('uniqueId1', 'assets/audio/' + this.urlAudio).then((success) => {
        // console.log("success");
        this.nativeAudio.play('uniqueId1');
        this.estado = 1;
      }, (error) => {

      });
    } else {
      this.nativeAudio.stop('uniqueId1').then((onSuccess) => {
        this.nativeAudio.unload('uniqueId1').then((onSuccess) => {
          console.log('desmontado');
          this.estado = 0;
        }, (onError) => {
          console.log('error');

        });
      }, (onError) => {
        console.log('error');

      });

    }

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.nativeAudio.stop('uniqueId1').then((onSuccess) => {
      this.nativeAudio.unload('uniqueId1').then((onSuccess) => {
        console.log('desmontado');
        this.estado = 0;
      }, (onError) => {
        console.log('error');

      });
    }, (onError) => {
      console.log('error');
    });
    console.log('salio de la vista');
    
  }

}
